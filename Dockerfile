FROM debian:stretch-slim as builder

RUN apt-get update && apt-get install -y \
	libcap2-bin \
	sudo \
	build-essential \
	gcc-multilib \
	g++-multilib \
	git \
	autoconf \
	ccache \
	bc \
	libssl-dev \
	libboost-dev \
	libboost-test-dev \
	libncurses-dev \
	zlib1g-dev \
	wget \
	python3.5 \
	python3-pip \
	python3-venv \
	libsqlite3-dev \
	cmake \
	libyaml-dev \
	procps \
	pigz \
	zip \
	sshpass \
	cppcheck \
	shellcheck \
	mosquitto mosquitto-clients \
	iputils-ping \
	&& apt-get clean

# Get and build GCC11
RUN cd /tmp ; \
	wget https://ftp.gnu.org/gnu/gcc/gcc-11.1.0/gcc-11.1.0.tar.gz ; \
	tar xzf gcc-11.1.0.tar.gz ; \
	cd gcc-11.1.0 ; \
	./contrib/download_prerequisites ; \
	mkdir objdir ; \
	cd objdir ; \
	../configure --enable-languages=c,c++ --target=x86_64-linux-gnu --host=x86_64-linux-gnu --build=x86_64-linux-gnu ; \
	make -j 8 ; \
	make install ; \
	rm -rf /tmp/*

ENV PATH "$PATH:/root/.local/bin"
ENV LD_LIBRARY_PATH=/opt/test/lib:/usr/local/lib:/usr/lib

RUN echo 'git-lfs' && \
	apt-get update && \
	apt-get install -y sudo curl zip && \
	curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash && \
	sudo apt-get install git-lfs && \
	mkdir -p /src \
	&& apt-get clean

# Set up libfaketime for unit testing.
RUN echo 'libfaketime' && \
	mkdir -p /opt/test/lib && \
	git clone --branch v0.9.8 --depth 1 https://github.com/wolfcw/libfaketime /tmp/libfaketime && \
	cd /tmp/libfaketime && \
	make && \
	cp src/libfaketime.so.1 /opt/test/lib/ && \
	cd .. && \
	rm -rf libfaketime

# Get and build OpenSSL from source
RUN echo 'OpenSSL' && \
	cd /tmp ; \
	git clone --branch OpenSSL_1_1_1d --depth 1 https://github.com/openssl/openssl.git ; \
	cd openssl ; \
	./config -Wl,--enable-new-dtags shared no-ssl2 ; \
	make ; \
	make install ; \
	mkdir -p /opt/test/lib/ ; \
	cp libcrypto* /opt/test/lib/ ; \
	cp libssl* /opt/test/lib/ ; \
	cd .. ; \
	rm -rf /tmp/openssl ;

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

CMD ["bash"]
