""" Simple Test """
import unittest


class TestExample(unittest.TestCase):
    """ Test Example """

    def test_boolean(self):
        """ Test boolean is equal """
        a_true = True
        b_true = True
        self.assertEqual(a_true, b_true)


if __name__ == '__main__':
    unittest.main()
